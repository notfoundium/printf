/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_x.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/22 21:55:43 by gmegga            #+#    #+#             */
/*   Updated: 2020/07/27 00:12:48 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		print_x(unsigned int x, int wid, int prec, unsigned int flags)
{
	char	str[16];
	int		len;
	int		nulls;
	int		spaces;

	if ((flags & WIDTH_BY_ZERO) && !(flags & (PRECISION | LEFT_ALIGNED))
																&& wid > 0)
	{
		prec = wid;
		wid = 0;
	}
	len = ft_xtos(x, str, 0);
	nulls = (len < prec) ? (prec - len) : (0);
	spaces = (len + nulls < wid) ? (wid - len - nulls) : (0);
	if (!(flags & LEFT_ALIGNED))
		set_align(' ', spaces);
	set_align('0', nulls);
	write(1, str, len);
	if (flags & LEFT_ALIGNED)
		set_align(' ', spaces);
	return (len + nulls + spaces);
}

int		print_u_x(unsigned int x, int wid, int prec, unsigned int flags)
{
	char	str[16];
	int		len;
	int		nulls;
	int		spaces;

	if ((flags & WIDTH_BY_ZERO) && !(flags & (PRECISION | LEFT_ALIGNED))
																&& wid > 0)
	{
		prec = wid;
		wid = 0;
	}
	len = ft_xtos(x, str, 1);
	nulls = (len < prec) ? (prec - len) : (0);
	spaces = (len + nulls < wid) ? (wid - len - nulls) : (0);
	if (!(flags & LEFT_ALIGNED))
		set_align(' ', spaces);
	set_align('0', nulls);
	write(1, str, len);
	if (flags & LEFT_ALIGNED)
		set_align(' ', spaces);
	return (len + nulls + spaces);
}

int		print_pointer(void *p, int wid, int prec, unsigned int flags)
{
	char	str[16];
	int		len;
	int		nulls;
	int		spaces;

	if ((flags & WIDTH_BY_ZERO) && !(flags & (PRECISION | LEFT_ALIGNED)))
	{
		prec = wid - 2;
		wid = 0;
	}
	len = ft_ulltos((unsigned long long)p, str);
	nulls = (len < prec) ? (prec - len) : (0);
	spaces = (len + 2 + nulls < wid) ? (wid - len - 2 - nulls) : (0);
	if (!(flags & LEFT_ALIGNED))
		set_align(' ', spaces);
	write(1, "0x", 2);
	set_align('0', nulls);
	write(1, str, len);
	if (flags & LEFT_ALIGNED)
		set_align(' ', spaces);
	return (len + 2 + nulls + spaces);
}
