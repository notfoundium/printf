/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/22 22:37:53 by gmegga            #+#    #+#             */
/*   Updated: 2020/07/26 23:28:21 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

const char	*arg_parser(va_list ap, const char *fmt, t_params *params)
{
	fmt = read_flags(fmt, &(params->flags));
	fmt = read_num(fmt, &(params->wid));
	params->prec = 1;
	if (*fmt == '.')
	{
		++fmt;
		if (*fmt == '-')
		{
			fmt = read_num(++fmt, &(params->prec));
			params->prec = 0;
		}
		else
			fmt = read_num(fmt, &(params->prec));
		params->flags |= PRECISION;
	}
	(params->wid < 0) ? (params->wid = va_arg(ap, int)) : (0);
	(params->wid < 0) ? (params->flags |= LEFT_ALIGNED) : (0);
	(params->wid < 0) ? (params->wid = -params->wid) : (0);
	(params->prec < 0) ? (params->prec = va_arg(ap, int)) : (0);
	(params->prec < 0) ? (params->flags &= ~PRECISION) : (0);
	(params->prec < 0) ? (params->prec = 1) : (0);
	return (fmt);
}

const char	*p_case(va_list ap, const char *fmt, size_t *len, t_params *p)
{
	if (*fmt == 'i' || *fmt == 'd')
		*len += print_int(va_arg(ap, int), p->wid, p->prec, p->flags);
	else if (*fmt == 'u')
		*len += print_uint(va_arg(ap, unsigned int),
											p->wid, p->prec, p->flags);
	else if (*fmt == 'x')
		*len += print_x(va_arg(ap, unsigned int), p->wid, p->prec, p->flags);
	else if (*fmt == 'X')
		*len += print_u_x(va_arg(ap, unsigned int), p->wid, p->prec, p->flags);
	else if (*fmt == 'c')
		*len += print_char(va_arg(ap, int), p->wid, p->flags);
	else if (*fmt == 's')
		*len += print_string(va_arg(ap, const char*),
													p->wid, p->prec, p->flags);
	else if (*fmt == 'p')
		*len += print_pointer(va_arg(ap, void *), p->wid, p->prec, p->flags);
	else if (*fmt == '%')
		*len += print_char('%', p->wid, p->flags);
	else
	{
		va_end(ap);
		return (NULL);
	}
	return (++fmt);
}

int			ft_printf(const char *fmt, ...)
{
	va_list		ap;
	size_t		len;
	t_params	params;

	len = 0;
	va_start(ap, fmt);
	while (*fmt)
	{
		if (*fmt == '%')
		{
			fmt = arg_parser(ap, ++fmt, &params);
			if (!(fmt = p_case(ap, fmt, &len, &params)))
				return (-1);
		}
		else
		{
			ft_putchar_fd(*fmt, 1);
			++len;
			fmt++;
		}
	}
	va_end(ap);
	return (len);
}

const char	*read_flags(const char *str, unsigned int *flags)
{
	*flags = 0;
	while (*str == '0' || *str == '-')
	{
		if (*str == '0')
			*flags |= WIDTH_BY_ZERO;
		else if (*str == '-')
			*flags |= LEFT_ALIGNED;
		str++;
	}
	return (str);
}

const char	*read_num(const char *str, int *num)
{
	*num = 0;
	if (*str == '*')
	{
		*num = -1;
		return (++str);
	}
	while (*str >= '0' && *str <= '9')
	{
		*num = *num * 10 + (*str - '0');
		str++;
	}
	return (str);
}
