/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/22 22:25:08 by gmegga            #+#    #+#             */
/*   Updated: 2020/07/27 00:06:59 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	set_align(int filler, int count)
{
	while (count-- > 0)
		ft_putchar_fd(filler, 1);
}

int		ft_itos(unsigned int n, char *str)
{
	size_t	len;
	char	c;
	int		next;

	if (n == 0)
		return (0);
	len = 1;
	c = '0' + n % 10;
	n /= 10;
	next = ft_itos(n, str);
	len += next;
	str += next;
	*str++ = c;
	return (len);
}

int		ft_xtos(unsigned int x, char *str, int size)
{
	size_t	len;
	char	c;
	int		next;
	int		mod;

	if (x == 0)
		return (0);
	len = 1;
	mod = x % 16;
	if (mod < 10)
		c = '0' + mod;
	else
		c = ((size) ? ('A') : ('a')) + mod - 10;
	x /= 16;
	next = ft_xtos(x, str, size);
	len += next;
	str += next;
	*str++ = c;
	return (len);
}

int		ft_ulltos(unsigned long long p, char *str)
{
	size_t	len;
	char	c;
	int		next;
	int		mod;

	if (p == 0)
		return (0);
	len = 1;
	mod = p % 16;
	if (mod < 10)
		c = '0' + mod;
	else
		c = 'a' + mod - 10;
	p /= 16;
	next = ft_ulltos(p, str);
	len += next;
	str += next;
	*str++ = c;
	return (len);
}
