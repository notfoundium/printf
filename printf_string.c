/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_string.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/22 22:32:41 by gmegga            #+#    #+#             */
/*   Updated: 2020/07/25 23:43:55 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			print_char(int c, int wid, unsigned int flags)
{
	int		len;

	len = (wid < 1) ? (1) : (wid);
	if (!(flags & LEFT_ALIGNED))
		set_align(' ', wid - 1);
	ft_putchar_fd(c, 1);
	if (flags & LEFT_ALIGNED)
		set_align(' ', wid - 1);
	return (len);
}

int			print_string(const char *str, int wid, int prec, unsigned int flags)
{
	int		len;

	len = 0;
	if (!str)
	{
		len = 6;
		if (flags & PRECISION)
			len = (prec > len) ? (len) : (prec);
		str = "(null)";
	}
	else
		while ((!(flags & PRECISION) || (len < prec)) && str[len])
			len++;
	if (!(flags & LEFT_ALIGNED))
		set_align(' ', wid - len);
	write(1, str, len);
	if (flags & LEFT_ALIGNED)
		set_align(' ', wid - len);
	return ((wid > len) ? (wid) : (len));
}
