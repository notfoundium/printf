# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gmegga <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/02/11 13:48:22 by gmegga            #+#    #+#              #
#    Updated: 2020/07/24 20:11:00 by gmegga           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

# C compiler parameters

CC = clang
CFLAGS = -Wall -Wextra -Werror
CSTD = -std=c99

# Sources

SOURCE = ft_printf.c\
		 printf_x.c\
		 printf_string.c\
		 printf_int.c\
		 printf_utils.c\
		 ft_putchar_fd.c

INCLUDES = ft_printf.h

OBJECTS = ft_printf.o\
		  printf_x.o\
		  printf_string.o\
		  printf_int.o\
		  printf_utils.o\
		  ft_putchar_fd.o

# Make rules

.PHONY: all clean fclean re norme so

all: $(NAME)

%.o: %.c
	@$(CC) $(CSTD) $(CFLAGS) $< -c

$(NAME): $(OBJECTS)
	@ar -rcs $(NAME) $(OBJECTS)
	@ranlib $(NAME)

clean:
	@rm -f $(OBJECTS)
	@rm -f *.gch

fclean:	clean
	@rm -f $(NAME)
	@rm -f *.so

re:
	@$(MAKE) fclean
	@$(MAKE) all

norme:
	@norminette $(SOURCE) $(INCLUDES)

so: 
	@gcc $(CFLAGS) $(CSTD) -fPIC -c $(SOURCE) $(BONUS) $(INCLUDES)
	@gcc -shared -o libft.so $(OBJECTS) $(INCLUDES)
