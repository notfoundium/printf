/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_int.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/22 22:27:05 by gmegga            #+#    #+#             */
/*   Updated: 2020/07/27 00:11:51 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			print_uint(unsigned int num, int wid, int prec, unsigned int flags)
{
	char	str[11];
	int		len;
	int		nulls;
	int		spaces;

	if ((flags & WIDTH_BY_ZERO) && !(flags & (PRECISION | LEFT_ALIGNED))
																	&& wid > 0)
	{
		prec = wid;
		wid = 0;
	}
	len = ft_itos(num, str);
	nulls = (len < prec) ? (prec - len) : (0);
	spaces = (len + nulls < wid) ? (wid - len - nulls) : (0);
	if (!(flags & LEFT_ALIGNED))
		set_align(' ', spaces);
	set_align('0', nulls);
	write(1, str, len);
	if (flags & LEFT_ALIGNED)
		set_align(' ', spaces);
	return (len + nulls + spaces);
}

int			print_int(int num, int wid, int prec, unsigned int flags)
{
	char	str[11];
	int		len;
	int		nulls;
	int		spaces;
	int		isneg;

	if ((flags & WIDTH_BY_ZERO) && !(flags & (PRECISION | LEFT_ALIGNED))
																	&& wid > 0)
	{
		prec = wid;
		wid = 0;
		(num < 0) ? (prec--) : (0);
	}
	isneg = (num < 0) ? (1) : (0);
	len = ft_itos((num < 0) ? (-num) : (num), str);
	nulls = (len < prec) ? (prec - len) : (0);
	spaces = (len + isneg + nulls < wid) ? (wid - len - isneg - nulls) : (0);
	if (!(flags & LEFT_ALIGNED))
		set_align(' ', spaces);
	(isneg) ? (ft_putchar_fd('-', 1)) : (0);
	set_align('0', nulls);
	write(1, str, len);
	if (flags & LEFT_ALIGNED)
		set_align(' ', spaces);
	return (len + isneg + nulls + spaces);
}
