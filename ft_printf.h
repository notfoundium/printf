/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/19 15:00:32 by gmegga            #+#    #+#             */
/*   Updated: 2020/07/27 00:07:34 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <unistd.h>
# include <stdarg.h>

typedef enum		e_flag
{
	WIDTH_BY_ZERO = 1 << 0,
	LEFT_ALIGNED = 1 << 1,
	PRECISION = 1 << 2
}					t_flag;

typedef	struct		s_params
{
	int				wid;
	int				prec;
	unsigned int	flags;
}					t_params;

void				ft_putchar_fd(char c, int fd);
int					ft_printf(const char *fmt, ...);
int					print_int(int num, int wid, int prec, unsigned int flags);
int					print_uint(unsigned int num,
										int wid, int prec, unsigned int flags);
int					print_x(unsigned int x,
										int wid, int prec, unsigned int flags);
int					print_u_x(unsigned int x,
										int wid, int prec, unsigned int flags);
int					print_char(int c, int wid, unsigned int flags);
int					print_string(const char *str,
										int wid, int prec, unsigned int flags);
int					print_pointer(void *p,
										int wid, int prec, unsigned int flags);
int					ft_itos(unsigned int n, char *str);
int					ft_xtos(unsigned int x, char *str, int size);
int					ft_ulltos(unsigned long long p, char *str);
const char			*read_flags(const char *str, unsigned int *flags);
const char			*read_num(const char *str, int *num);
void				set_align(int filler, int count);
#endif
